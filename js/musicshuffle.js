var midiUrl = "music/";
var midiName =
	["01-Simutrans-Main-Theme","02-Gotta-catch-that-train","03-Sunday-drivers","04-Simutrans-B-Theme",
	"05-Boring-afternoon","06-A-busy-day-at-the-depot","07-Transport-chaos","08-The-journey-home",
	"09-Simupolitan-Swing","10-Easy-driving","11-Stucked-Convoi","12-Steamin-across-the-prairies",
	"13-Stephenson-blues","14-Last-journey-of-the-Niagara","15-The-Wayside-Blues","16-Midnight-Express2",
	"17-The-Benevolent-Dictators-March","18-Ride-that-train","19-Rockin-trucker","20-Last-Trip",
	"21-Dusty-Eyes","22-Variable-Journeys","23-Something-for-Silver-Sand","24-needlessly-striking",
	"25-Float-on-by","26-Tantalizingly-Unusual","27-March-Winds","28-Road-to-Warm-Places",
	"29-Runaway","30-On-the-waterfront","31-Courtenay-Bridge","32-incidental-skies",
	"33-Journey-to-times-gone-by","34-flyingaway","35-deep-ride","36-faded-things",
	"37-inevitably-engrossed","38-positive-thrill","39-bangin-mover","40-alternative",
	"41-Libertador","42-Stranger-Echoes","43-Driving-on-the-midnight-highway","44-Above-the-sky",
	"45-Misty-Forest","46-House-in-the-station"];

var midiDesc =
	["ﾃﾚｰｰｰﾃﾚﾚﾚｰｰｰﾚｰｰｰﾃｰｰｰﾃﾚﾚﾚﾚｰｰｰﾚｰｰｰ","ｽﾞｽﾞﾝｽﾞﾝｽﾞｽﾞｽﾞｽﾞｽﾞﾝｽﾞﾝｽﾞｽﾞｽﾞﾃﾚﾚﾚﾚｰﾚｰﾃﾚﾚﾚﾚｰﾚｰﾃﾚﾚﾚﾚｰﾚｰﾚｰｰｰ",
	"ﾃﾞﾃﾞﾝﾃﾞﾝﾃﾞﾝﾃﾞﾝﾃﾞﾃﾞﾝﾃﾞﾝﾃﾞﾝﾁｬﾝﾁｬﾁｬﾝﾁｬ","ﾃｰｰｰﾃﾚﾚﾚﾃｰｰｰﾃｰｰｰﾃﾚﾚﾚｰｰｰﾃﾚﾚｰｰｰｰｰﾚ",
	"ﾌｧｰｰｰｰｰｰｰｰｰﾃｰｰﾝﾃｰｰﾝﾃﾚﾚﾚﾚｰｰﾝ","(ｼﾞｬｰﾝ)ﾃｰｰﾃﾃｰｰｰ(ｼﾞｬｰﾝ)ﾃｰｰﾃﾃｰｰｰ(ｼﾞｬｰﾝ)ﾃｰｰﾃﾃﾃﾃﾃ(ｼﾞｬｰﾝ)ﾃｰｰﾃﾃｰｰｰ",
	"ｽﾞﾝｽﾞｽﾞﾝｽﾞﾃﾝﾃﾚﾚﾚﾚﾚﾚﾚﾚﾃﾚﾚﾚﾃﾝ","ﾃｰﾃｰﾃｰｰｰﾃｰﾃｰﾃｰｰｰﾃﾚﾚﾚﾚﾚﾚｰﾃﾚﾚﾚﾚｰｰｰ",
	"ﾍﾟｰｹﾍﾟｰｹﾍﾟｰｹﾍﾟｰｹﾍﾟｰｰｰｰｰｰｰｰｰｰｰ","ｼﾞｬﾝｼﾞｬﾝｼﾞｬﾝｼﾞｬﾝﾌｧｰｰｰｰｰｰｰﾌｧｰｰｰｰｰｰｰﾌｧｰｰｰｰｰｰｰ",
	"ﾍﾞﾍﾞﾍﾞﾍﾞﾝﾍﾞﾍﾞﾍﾞﾍﾞﾍﾞﾍﾞﾍﾞｯﾍﾞｰｰ","ﾃﾚｰﾝ(ﾌｧｰｰｰｰｰｰｰｰｰｰｰｰｰｰｰ)",
	"ｺｹｰ","ｽﾞﾝﾁｬｽﾞｽﾞﾝﾁｬ(ﾌｧｰｰｰｰｰｰｰｰｰｰｰｰｰｰｰ)",
	"ﾍﾞﾍﾞﾝﾍﾞｰｰｰｰﾝ ﾍﾞﾍﾞﾝﾍﾞｰｰｰｰﾝ","ｽﾞﾝﾁｬｽﾞﾝﾁｬｽﾞﾝﾁｬｽﾞﾝﾁｬ",
	"ﾍﾟｯﾍﾟｹﾍﾟｰｰｰｰｰｰｰｰｰｰｰﾍﾟｯﾍﾟｹﾍﾟｰｰｰｰｰｰｰｰｰｰｰ","ｽﾞｯｽﾞﾃﾞﾝﾃﾞﾃﾞﾝﾃﾞﾃﾞﾚﾚｽﾞｯｽﾞﾃﾞﾝﾃﾞﾃﾞﾝﾃﾞﾃﾞﾚﾚ",
	"ﾁｬｰﾝﾁｬｰﾝﾁｬｰﾝ ﾁｬﾁｬｶﾁｬﾝﾁｬﾝﾁｬﾝﾁｬﾝﾁｬｰﾝﾁｬﾁｬｶﾁｬｶ","ｷﾗｷﾗｰﾝ ﾌｪｰﾝﾌｪｰﾝﾌｪｰﾝﾌｪｰｰｰﾝﾌｪｰｰｰﾝ",
	"ﾁｬﾗﾗﾁｬｰﾝ ﾁｬﾗﾁｬｰﾝ ﾁｬﾗﾁｬｰﾝ","ﾁｬｰﾝﾁｬｰﾝﾁｬｰﾝﾁｬｰﾝ ﾁｬｶﾁｬｰﾝﾁｬｰﾝ",
	"ｽﾞﾝﾁｬ ｽﾞﾝﾁｬ ﾃﾚｰﾚﾃｯﾃｯﾃｯﾃｯﾃｯﾃﾚｯﾃｯﾃﾃﾚﾚﾚｯﾃﾚﾚﾚ","ﾃｰﾚｰﾚｯｼﾞｬｯｼﾞｬｯｼﾞｬｰﾗｼﾞｬ",
	"ﾌｧｰｰｰｰｰｰｰﾌｧｰｰﾃﾚｰｰｰﾌｧｰｰｰｰｰｰｰﾌｧｰｰﾃﾚｰｰｰ","ｽﾞｯｽﾞｯｽﾞｯﾃﾃｰﾝﾃｰﾝﾃｰﾝ ﾃｰﾝ",
	"ﾃｰｰﾝ ﾃｰﾃｽﾞﾝﾁｬ ｽﾞﾝﾁｬ ｽﾞﾝﾁｬ ｽﾞﾝﾁｬ","ｽﾞﾝｽﾞﾝｽﾞﾝｽﾞﾝ ｽﾞｸﾞｽﾞﾝｽﾞﾝｽﾞﾝｽﾞﾝｽﾞﾝ",
	"ﾌﾞﾍﾞﾍﾞﾍﾞｯﾍﾞｯﾍﾞﾍﾞﾚｯﾍﾞﾍﾞﾚﾍﾞﾚ","ｽﾞﾝﾁｬｽﾞﾝﾁｬｽﾞﾝﾃｰｰﾃｰｰｰｰｰｰｰｰ",
	"ﾍﾟｰｰｰｰｰｰｰｰﾍﾟｰﾍﾟｰﾍﾟｰﾍﾟｰｰﾍﾟｰｰｰｰｰｰｰｰｰｰ","ｽﾞﾝﾁｬｽﾞﾝﾁｬ(ﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃﾃ)",
	"ｽﾞﾝﾁｬｽﾞﾝﾁｬ ﾁｬﾗｯﾁｬﾁｬﾗﾗｯﾁｬﾗｰﾁｬﾗﾗﾗｰ","ﾚﾚﾚﾚｰﾚｰﾚﾚﾚﾚﾚｰﾚｰﾚﾌｪｰｰｰｰｰｰｰｰｰｰｰ",
	"ﾌｫｰﾌｫｰﾝ ﾌｫｰﾝﾌｫｰﾝ","ｽﾞﾝﾁｬｽﾞﾝﾁｬ ﾃﾚｰﾃﾚｯﾃｰﾝ",
	"ｽﾞﾝﾁｬ ｽﾞﾝﾁｬ ｼﾞｬｼﾞｬﾝｼﾞｬﾝｼﾞｬﾝｼﾞｬｰﾝｼﾞｬｼﾞｬｰﾝｼﾞｬﾝｼﾞｬｰﾝ","ﾁｬｰﾝﾁｬｰﾝﾁｬｰﾗﾗｰﾗﾗｰﾗｰﾝ",
	"ｽﾞﾝﾁｬ ｽﾞﾝﾁｬ ﾃｯﾃﾚｰﾚ ﾃｰﾝﾃｯﾃｯﾃｯﾃｰｰﾚｰｰﾝ","ｳｰｰｼﾞｬﾞｰﾞｰﾞｰﾞｰﾞｰﾞｰﾞｰﾞｰﾞｰﾞﾝﾞ",
	"ｽﾞﾝｽﾞｸｽﾞｸｽﾞﾝﾊﾞｰﾝﾃﾚｰﾝ","ﾝﾎﾞﾎﾞﾝﾎﾞｰ",
	"ﾗｰﾁｬｰﾗｰﾁｬｰﾗｰﾁｬｰﾗｰﾁｬｰﾗｰ ﾁｬﾗﾗﾗﾁｬｰ","ﾃｰｰﾃ ﾃｰｰﾃ ﾃｰﾃｰｰﾃ",
	"ﾌｧｯﾌｧｯﾌｧｯﾌｧｯﾌｧｯﾌｧｯﾌｧｯﾌｧｰ","ｼﾞｪｲｱｰﾙﾋｶﾞｼﾆﾎﾝ"]

var rand;
var midiPlay;
var desc;
var playNumber = [];
var qn = 1;
var firstA;
var correct = 0;


function musicStart() {
	firstA = 0;
	var mFunc = 0;
	while (mFunc == 0) {
		rand = parseInt(Math.random()*midiName.length);
		var result = playNumber.indexOf(rand);
		if (result == (-1)) {
			playNumber.push(rand);
			mFunc = 1;
		} else if(playNumber.length == midiName.length) {
			mFunc = 2;
		}
	}

	if (mFunc == 1) {
		midiPlayTitle = midiUrl + midiName[rand] + ".mid";
		midiPlayStart(midiPlayTitle);
		desc = "曲名:"+ midiName[rand] +"\nｲﾝﾄﾛ:"+ midiDesc[rand];

		document.getElementById("midiArea").innerHTML = midiPlayTitle;
		document.getElementById("qNumber").innerHTML = "<b>★第"+qn+"問目★(ここまで"+correct+"問正解)</b>";
		qn+=1;
	} else if(mFunc == 2) {
		if ((correct/midiName.length) == 1.0) {
			document.getElementById("midiArea").innerHTML = "<img src=\"music/reti06.gif\"><br/>";
		} else if((correct/midiName.length) >= 0.7) {
			document.getElementById("midiArea").innerHTML = "<img src=\"music/reti05.gif\"><br/>";
		} else if((correct/midiName.length) >= 0.5) {
			document.getElementById("midiArea").innerHTML = "<img src=\"music/reti04.gif\"><br/>";
		} else if((correct/midiName.length) >= 0.25) {
			document.getElementById("midiArea").innerHTML = "<img src=\"music/reti03.gif\"><br/>";
		} else if((correct/midiName.length) >= 0.1) {
			document.getElementById("midiArea").innerHTML = "<img src=\"music/reti02.gif\"><br/>";
		} else {
			document.getElementById("midiArea").innerHTML = "<img src=\"music/reti01.gif\"><br/>";
		}

	}

}

function ansJudge(code) {
	 if(13 === code) {
		 var inputNumber = document.getElementsByName("inputNumber");
		 var answer = document.getElementsByName("answer");
		 var textDesc = document.getElementsByName("textDesc");

		 if (parseInt(inputNumber[0].value) == (parseInt(rand)+1)) {
			 answer[0].value  = "すげえ！正解^^";
		     textDesc[0].value = desc;
		     if (firstA == 0) {
		    	 correct+=1;
		     }
		 } else {
			 answer[0].value  = "><";
			 textDesc[0].value = desc;
			 firstA = 1;
		 }

	 }
	}
