===============================================
Shunter Practice -シムトライントロクイズ道場-
Ver 1.1
===============================================
▼はじめに
	このパッケージは、Simutrans BGM46曲のイントロクイズ大会Shunter League
	のトレーニング用として作成されたHTMLファイルです。
	なお、Mozilla Firefox, Google Chrome, Internet Explorer対象です。
	Microsoft Edgeでは動作しません。
	Safariは環境無いため未確認です。

▼主要ファイル・ディレクトリ説明
	○Readme.txt	:	このファイル
	○index.html	:	Firefox, Google Chrome等用（MIDI.js仕様）
	○main.html		:	Internet Explorer用
						（Windows Media Playerプラグイン使用）
	○soundfont/	:	MIDI.js用音源フォルダ
	○music/		:	Simutrans BGMフォルダ

▼使い方
	1) index.html又はmain.htmlを開いてください。
		（index.html版はサーバ上においてアクセスする形が確実に動作します）
	2) [次の問題]をクリックしてください。
	3) 回答欄上に「★第1問目★」と表示され、音楽が再生されます。
	   楽曲の番号（Simutrans Main Themeであれば1）を回答欄へ入力し、Enter
	   を押してください。
	4) 判定が出ますので、確認後[次の問題]を押してください。
	5) 以下、全出題終了まで3) - 4)を繰り返します。
	6) 最終問題終了後、正答率が表示されます。

▼改版履歴
	Ver 1.0 : 初版作成 
				作成 : 五番星(@gobanboshi)
	Ver 1.1 : MIDI.js版作成
				作成 : 廉(@osukoke)
